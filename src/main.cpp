#include <iostream>
#include <string.h>

/* Function prototypes */
void encrypt(char* text);
void decrypt(char* text);

int main(int argc, char **argv)
{
	/* Process command line arguments */
	for(int i(0); i < argc; ++i)
	{
		if((std::string)argv[i] == "-e" || (std::string)argv[i] == "--encrypt")
		{
			encrypt(argv[i+1]);
		}

		if((std::string)argv[i] == "-d" || (std::string)argv[i] == "--decrypt")
		{
			decrypt(argv[i+1]);
		}

		if((std::string)argv[i] == "-h" || (std::string)argv[i] == "--help")
		{
			std::cout << "\nUsage:  [OPTION] \"TEXT\""
				<< "\n Options.\n  -e, --encrypt\t\tEncrypt text."
				<< "\n  -d, --decrypt\t\tDecrypt text.\n\n";
		}
	}

	/* Display help text if no arguments are found */
	if(argc < 2)
	{
		 std::cout << "\nUsage:  [OPTION] \"TEXT\""
                	   << "\n Options.\n  -e, --encrypt\t\tEncrypt text."
                           << "\n  -d, --decrypt\t\tDecrypt text.\n\n";
	}


	return 0;
}

/* Multi dimensional array containing the alphabet Lower and Upper case */
char alpha[26][2]
{
	{'A','a'}, {'B','b'}, {'C','c'},
	{'D','d'}, {'E','e'}, {'F','f'},
	{'G','g'}, {'H','h'}, {'I','i'},
	{'J','j'}, {'K','k'}, {'L','l'},
	{'M','m'}, {'N','n'}, {'O','o'},
	{'P','p'}, {'Q','q'}, {'R','r'},
	{'S','s'}, {'T','t'}, {'U','u'},
	{'V','v'}, {'W','w'}, {'X','x'},
	{'Y','y'}, {'Z','z'}
};

void encrypt(char* text)
{
	char cEncrypted[strlen(text)];

	/* Loop for text */
	for(int i(0); i < strlen(text); i++)
	{
		/* Loop for lower or uppercase */
		for(int i2(0); i2 < 26; i2++)
		{
			/* Text replace loop */
			for(int i3(0); i3 < 2; i3++)
			{
				/* If exceeds end of array start at beginning */
				if(i2+4 > 26)
				{
					if(text[i] == ' ') cEncrypted[i] = ' ';
					else
					{
						if(alpha[i2][i3] == text[i]) cEncrypted[i] = alpha[i2-26+4][i3];
					}
				}
				else
				{
					if(text[i] == ' ') cEncrypted[i] = ' ';
					{
						if(alpha[i2][i3] == text[i]) cEncrypted[i] = alpha[i2+4][i3];
					}
				}
			}
		}
	}

	/* Prints out encrypted message */
	std::cout << cEncrypted << "\n";
}

void decrypt(char* text)
{
	char cDecrypted[strlen(text)];

	/* Loop for text */
	for(int i(0); i < strlen(text); i++)
	{
		/* Loop for lower or uppercase */
		for(int i2(26); i2 > 0; i2--)
		{
			/*Text replace loop */
			for(int i3(0); i3 < 2; i3++)
			{
				/* If exceeds beginning of array start at end */
				if(i2-4 < 0)
				{
                                        if(text[i] == ' ') cDecrypted[i] = ' ';
					else
					{
						if(alpha[i2][i3] == text[i]) cDecrypted[i] = alpha[i2+26-4][i3];
					}
				}
				else
				{
					if(text[i] == ' ') cDecrypted[i] = ' ';
					else
					{
						if(alpha[i2][i3] == text[i]) cDecrypted[i] = alpha[i2-4][i3];
					}
				}
			}
		}
	}

	/* Prints out decrypted message */
	std::cout << cDecrypted << "\n";
}
